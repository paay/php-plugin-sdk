### Installation
    
Download package from [here](https://bitbucket.org/paay/php-plugin-sdk/get/a2e6757e4dc9.zip),
unpack it and copy all 'paay_lib_production' directory to directory with your project.
    
To start work with PAAY package just insert the following lines to your project:
    
```php
require_once('paay_lib_production/init.php');
```

The you can use PAAY Class to make PAAY transactions

```php
use PAAY\Plugin\PaayApiPlugin;
```

### Example of use
    
_Note: All examaple available [here](https://bitbucket.org/paay/php-plugin-sdk/src/130e0e48d3188e6f2cf8e03580944da0b78697a3/examples/?at=master)_

The simples way to use PAAY package to create PAAY Transaction is use only PaayApiPlugin object
to connect with PAAY API and get transaction url address to use in your application.

Suppose that you have shop with material products. Your Client placed an order so you have
information from cart e.g.

```php
$basicParameters = array(
    'amount'    => 3.33,
    'orderId'   => 1,
    'details'   => json_encode(array(
        array("name" => "Test Product 1", "amount" => 2, "cost" => 1.11),
        array("name" => "Test Product 2", "amount" => 1, "cost" => 2.22)
    )),
    'email'     => 'test@example.com',
);
```

Then you can fill in the rest of the basic information data coming from the shop settings

```php
$basicParameters['returnUrl'] = 'http://example.com/success';
$basicParameters['cancelUrl'] = 'http://example.com/cancel/1';
$basicParameters['statusUrl'] = 'http://example.com/approve_transaction?orderId=1';
$basicParameters['threeds_visibility'] = 'detected'
```

After that you can fill information about customer like shipping and billing address
_Note: this step is optional_

```php
$billingAddressParameters = array(
    'billingFirstName'  => 'Jon',
    'billingLastName'   => 'Doe',
    'billingCompany'    => '',
    'billingEmail'      => 'test@example.com',
    'billingAddress1'   => 'Main Street',
    'billingAddress2'   => '11A',
    'billingCity'       => 'New York',
    'billingPostcode'   => '41165',
    'billingState'      => 'NY',
    'billingCountry'    => 'USA',
    'billingPhone'      => '111222333',
);

$shippingAddressParameters = array(
    'shippingFirstName' => 'Jon',
    'shippingLastName'  => 'Doe',
    'shippingAddress1'  => 'Main Street',
    'shippingAddress2'  => '11A',
    'shippingCity'      => 'New York',
    'shippingPostcode'  => '41165',
    'shippingState'     => 'NY',
    'shippingCountry'   => 'USA',
    'shippingPhone'     => '111222333',
);
```

When you have all iformation about order and client then you can send request about transaction url
to PAAY API.

_Note: setAuthorizationParameters() method require your API Key and API Secret_

_Note: getTransactionUrl() method require PAAY API Host and it should be taken from shop settings_

```php
try {
    $api = new PaayApiPlugin();

    $api->setBasicParameters($basicParameters);
    $api->setBillingParameters($billingAddressParameters);
    $api->setShippingParameters($shippingAddressParameters);

    $api->setAuthorizationParameters('merc_abc_key', 'merc_abc_secret');

    $url = $api->getTransactionUrl('http://api2.paay.dock');

} catch(\Exception $e){
    echo 'ERROR: ' . $e->getMessage();
}
```

If you got PAAY transaction url then you can display the user PAAY transaction landing page inside
e.g. iframe or just redirect him to landing page directly.

### Example of use (another way)

During creating PAAY transaction you can use our classes which help you create transaction easier.

First you can use Setting class to have an access to your individual settings. Only you have
to do is define settings key e.g.:

```php
use PAAY\Plugin\Helpers\Settings;
use PAAY\Plugin\Helpers\SettingsInterface;

class ShopSettings extends Settings implements SettingsInterface
{
    const PAAY_HOST             = 'paay_host';
    const PAAY_KEY              = 'paay_key';
    const PAAY_SECRET           = 'paay_secret';
    const PAAY_PAYMENT_STRATEGY = 'paay_payment_strategy';
    const PAAY_IFRAME_OPTION    = 'paay_iframe_option';
    const PAAY_RETURN_URL       = 'paay_return_url';
    const PAAY_CANCEL_URL       = 'paay_cancel_url';
    const PAAY_STATUS_URL       = 'paay_status_url';

    const RETURN_URL = 'transaction/success';
    const CANCEL_URL = 'it will not be used';
    const STATUS_URL = 'transaction/approve_transaction';

    public function __construct($settings, $baseUrl)
    {
        $this->settings = $settings;
        $this->baseUrl = $baseUrl;
    }
}
```

After that you have to define you own class which extracts data from e.g. order data.
It might look like the following:

```php
use PAAY\Plugin\Helpers\DataExcavator;
use PAAY\Plugin\Helpers\DataExcavatorInterface;

class ShopExcavator extends DataExcavator implements DataExcavatorInterface
{
    private $order;

    public function setOrder($order)
    {
        $this->order = $order;
    }

    public function excavateBasicParameters()
    {
        $shopData = array(
            'amount' => $this->order['amount'],
            'orderId' => $this->order['orderId'],
            'details' => $this->details($this->order['details']),
            'email' => $this->order['email']
        );

        return $this->fillSettingsField($shopData, $this->order['orderId']);
    }

    private function details(array $items)
    {
        $details = array();

        foreach($items as $item){
            $details[] = array(
                'name' => $item['name'],
                'cost' => $item['cost'],
                'amount' => $item['amount']
            );
        }

        return json_encode($details);
    }

    public function excavateBillingParameters()
    {
        return array(
            'billingFirstName'  => $this->order['billingFirstName'],
            'billingLastName'   => $this->order['billingLastName'],
            'billingCompany'    => $this->order['billingCompany'],
            'billingEmail'      => $this->order['billingEmail'],
            'billingAddress1'   => $this->order['billingAddress1'],
            'billingAddress2'   => $this->order['billingAddress2'],
            'billingCity'       => $this->order['billingCity'],
            'billingPostcode'   => $this->order['billingPostcode'],
            'billingState'      => $this->order['billingState'],
            'billingCountry'    => $this->order['billingCountry'],
            'billingPhone'      => $this->order['billingPhone'],
        );
    }

    public function excavateShippingParameters()
    {
        return array(
            'shippingFirstName'  => $this->order['shippingFirstName'],
            'shippingLastName'   => $this->order['shippingLastName'],
            'shippingAddress1'   => $this->order['shippingAddress1'],
            'shippingAddress2'   => $this->order['shippingAddress2'],
            'shippingCity'       => $this->order['shippingCity'],
            'shippingPostcode'   => $this->order['shippingPostcode'],
            'shippingState'      => $this->order['shippingState'],
            'shippingCountry'    => $this->order['shippingCountry'],
            'shippingPhone'      => $this->order['shippingPhone'],
        );
    }
}
```

If you define this two classes then you can create PAAY Transaction:

```php
use PAAY\Plugin\PaayApiPlugin;

$settings = [
    'paay_key' => 'test_key',
    'paay_secret' => 'test_secret',
    'paay_payment_strategy' => 'redirect',
    'paay_cancel_url' => 'payment/custom/url',
];
$baseUrl = 'http://example.com';

$settings = new ShopSettings($settings, $baseUrl);

$order = array(
    'amount'    => 3.33,
    'orderId'   => 1,
    'details'   => array(
        array("name" => "Test Product 1", "amount" => 2, "cost" => 1.11),
        array("name" => "Test Product 2", "amount" => 1, "cost" => 2.22)
    ),
    'email'     => 'test@example.com',
    'billingFirstName'  => 'Jon',
    'billingLastName'   => 'Doe',
    'billingCompany'    => '',
    'billingEmail'      => 'test@example.com',
    'billingAddress1'   => 'Main Street',
    'billingAddress2'   => '11A',
    'billingCity'       => 'New York',
    'billingPostcode'   => '41165',
    'billingState'      => 'NY',
    'billingCountry'    => 'USA',
    'billingPhone'      => '111222333',
    'shippingFirstName' => 'Jon',
    'shippingLastName'  => 'Doe',
    'shippingAddress1'  => 'Main Street',
    'shippingAddress2'  => '11A',
    'shippingCity'      => 'New York',
    'shippingPostcode'  => '41165',
    'shippingState'     => 'NY',
    'shippingCountry'   => 'USA',
    'shippingPhone'     => '111222333',
);

$excavator = new ShopExcavator();

$excavator->setSettings($settings);
$excavator->setOrder($order);

$api = new PaayApiPlugin();
$api->setParameters($excavator);
$api->setAuthorizationParameters('merc_abc_key', 'merc_abc_secret');

$url = $api->getTransactionUrl('http://api2.paay.dock');
```

### Approve transaction

After completing the process, transaction needs to be approve. To achive it API PAAY
after approve transaction sends a request to you application to the address whit you
entered in 'STATUS_URL'.
You have to handle this request, check authorization data and if everythink is correct
diplay word 'OK'

Exampled implementation

```php
$paayApi = new PaayApiPlugin();

// settings object is only example in real app you should use your own settings class
$settings = new ShopSettings(array(), null);

// you should't use POST data directly
$post = $_POST;
$signature = (isset($post['signature'])) ? $post['signature'] : null;


if(!$paayApi->checkSignature($settings->key(), $settings->secret(), $post, $signature)){
    throw new \Exception('You are Unauthorized');
}

// code with change e.g. order status

echo 'OK';
```