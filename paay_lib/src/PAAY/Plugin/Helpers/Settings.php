<?php

namespace PAAY\Plugin\Helpers;

abstract class Settings
{
    const ORDER_STATUS_PENDING  = 'PAAY Pending';
    const ORDER_STATUS_APPROVED = 'PAAY Approved';

    protected $settings;
    protected $baseUrl;

    public function host()
    {
        return $this->value(static::PAAY_HOST);
    }

    public function key()
    {
        return $this->value(static::PAAY_KEY);
    }

    public function secret()
    {
        return $this->value(static::PAAY_SECRET);
    }

    public function paymentStrategy()
    {
        $value = $this->value(static::PAAY_PAYMENT_STRATEGY);

        return ($value === false) ? 'modal' : $value;
    }

    public function iframeOption()
    {
        $value = $this->value(static::PAAY_IFRAME_OPTION);

        return ($value === false) ? 'never' : $value;
    }

    public function returnUrl($orderId = null)
    {
        return $this->urlValue($orderId, static::PAAY_RETURN_URL, static::RETURN_URL);
    }

    public function cancelUrl($orderId = null)
    {
        return $this->urlValue($orderId, static::PAAY_CANCEL_URL, static::CANCEL_URL);
    }

    public function statusUrl($orderId = null)
    {
        return $this->urlValue($orderId, static::PAAY_STATUS_URL, static::STATUS_URL);
    }

    public function extensionsEnabled()
    {
        return function_exists('curl_version');
    }

    protected function urlValue($orderId, $defaultAdjustedKey, $defaultValue)
    {
        $idPart = ($orderId === null) ? '' : '?orderId=' . $orderId;
        $adjustedValue = $this->value($defaultAdjustedKey);

        if($adjustedValue){
            return $this->baseUrl .'/'. trim($adjustedValue, '/') . $idPart;
        }

        return $this->baseUrl .'/'. $defaultValue . $idPart;
    }

    protected function value($key)
    {
        return (empty($this->settings[$key])) ? false : $this->settings[$key];
    }
}
