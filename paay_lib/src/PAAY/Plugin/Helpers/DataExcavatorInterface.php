<?php

namespace PAAY\Plugin\Helpers;

interface DataExcavatorInterface
{
    public function setSettings(SettingsInterface $settings);

    public function excavateBasicParameters();

    public function excavateBillingParameters();

    public function excavateShippingParameters();
}