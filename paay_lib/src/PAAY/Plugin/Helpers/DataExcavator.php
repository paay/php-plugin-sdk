<?php

namespace PAAY\Plugin\Helpers;

abstract class DataExcavator
{
    protected $settings;

    public function setSettings(SettingsInterface $settings)
    {
        $this->settings = $settings;

        return $this;
    }

    protected function fillSettingsField(array $data, $orderId = null)
    {
        $data['returnUrl'] = $this->settings->returnUrl($orderId);
        $data['cancelUrl'] = $this->settings->cancelUrl($orderId);
        $data['statusUrl'] = $this->settings->statusUrl($orderId);
        $data['threeds_visibility'] = $this->settings->iframeOption();
        
        return $data;
    }
}
