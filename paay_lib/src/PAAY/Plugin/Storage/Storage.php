<?php

namespace PAAY\Plugin\Storage;

use PAAY\Plugin\Authorization\Signature;
use PAAY\Plugin\Exceptions\LockStorageException;
use PAAY\Plugin\Settings\Settings;
use PAAY\Plugin\Storage\Component\StorageComponent;
use PAAY\Plugin\Storage\Component\StorageComponentInterface;

class Storage extends StorageComponent implements StorageInterface
{
    protected $key = Settings::AUTHORIZATION_FIELDS;
    protected $lock = false;
    protected $data = array();

    public function completeFillingUp($key, $secret)
    {
        $this->data['api_key'] = $key;

        $signature = new Signature($key, $secret);
        $this->data['signature'] = $signature->get($this->data);

        foreach ($this->settings as $field => $attributes) {
            $this->requireValue($this->data, $field, (bool)$attributes['required']);
            $this->checkType($this->data, $field, $attributes['type']);
        }

        $this->lock = true;
        return true;
    }
    
    public function add(StorageComponentInterface $storageComponent)
    {
        if($this->lock){
            throw new LockStorageException('You cannot add components after create API signature');
        }

        $this->data = array_merge($this->data, $storageComponent->data());

        return $this;
    }
}
