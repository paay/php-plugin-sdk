<?php

namespace PAAY\Plugin;

use PAAY\Plugin\Client\ApiStandalone;
use PAAY\Plugin\Helpers\DataExcavatorInterface;
use PAAY\Plugin\Storage\Component\Basic;
use PAAY\Plugin\Storage\Component\BillingAddress;
use PAAY\Plugin\Storage\Component\ShippingAddress;
use PAAY\Plugin\Storage\Storage;
use PAAY\Plugin\Authorization\Signature;

class PaayApiPlugin implements PaayApiPluginInterface
{
    private $storage;
    private $api;
    
    public function __construct()
    {
        $this->storage = new Storage();
        $this->api = new ApiStandalone();
    }

    public function setBasicParameters(array $data)
    {
        $basicParameters = new Basic();
        $basicParameters->fill($data);
        
        $this->storage->add($basicParameters);
    }

    public function setBillingParameters(array $data)
    {
        $billingParameters = new BillingAddress();
        $billingParameters->fill($data);
        
        $this->storage->add($billingParameters);
    }

    public function setShippingParameters(array $data)
    {
        $shippingParameters = new ShippingAddress();
        $shippingParameters->fill($data);
        
        $this->storage->add($shippingParameters);
    }
    
    public function setParameters(DataExcavatorInterface $excavator)
    {
        $this->setBasicParameters($excavator->excavateBasicParameters());
        $this->setBillingParameters($excavator->excavateBillingParameters());
        $this->setShippingParameters($excavator->excavateShippingParameters());
    }

    public function setAuthorizationParameters($key, $secret)
    {
        $this->storage->completeFillingUp($key, $secret);
    }
    
    public function getTransactionUrl($host, $strategy = 'modal')
    {
        return $this->api->transactionUrl($host, $this->storage, $strategy);
    }

    public function checkSignature($key, $secret, $payload, $signature)
    {
        $sig = new Signature($key, $secret);
        return $sig->get($payload) === $signature;
    }
}
