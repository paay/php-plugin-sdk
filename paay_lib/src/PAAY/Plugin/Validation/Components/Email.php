<?php

namespace PAAY\Plugin\Validation\Components;

class Email implements ValidatorInterface
{
    public function valid($value)
    {
        if (filter_var($value, FILTER_VALIDATE_EMAIL)){
            return true;
        }
        return false;
    }
}