<?php

require __DIR__.'/vendor/symfony/class-loader/Psr4ClassLoader.php';
require __DIR__.'/vendor/autoload.php';

use Symfony\Component\ClassLoader\Psr4ClassLoader;

$loader = new Psr4ClassLoader();
$loader->addPrefix('PAAY\\Plugin\\', __DIR__.'/src/PAAY/Plugin');
$loader->register();
