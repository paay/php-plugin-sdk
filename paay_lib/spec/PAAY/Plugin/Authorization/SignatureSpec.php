<?php

namespace spec\PAAY\Plugin\Authorization;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class SignatureSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('test_key', 'test_secret');
    }
    
    function it_is_initializable()
    {
        $this->shouldHaveType('PAAY\Plugin\Authorization\Signature');
    }

    function it_should_return_signature_string()
    {
        $this->get(array())->shouldBeString();
        $this->get(array())->shouldEqual('88f1aa6ffeb47f1c0d61594a261caa8a1da58025ac4eb22cb0e29638b69cabb9');
    }
}
