<?php

namespace spec\PAAY\Plugin\Storage;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use PAAY\Plugin\Exceptions\LockStorageException;
use PAAY\Plugin\Exceptions\EmptyFieldAttributeException;
use PAAY\Plugin\Storage\Component\StorageComponentInterface;

class StorageSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('PAAY\Plugin\Storage\Storage');
    }

    function it_should_return_true_after_fill_the_object()
    {
        $apiKey = 'test';
        $apiSecret = 'test';

        $this->completeFillingUp($apiKey, $apiSecret)->shouldReturn(true);
    }

    function it_should_throw_lock_storage_exception_during_add_arguments_after_filling(StorageComponentInterface $component)
    {
        $component->data()->willReturn(array());
        $this->completeFillingUp('test', 'test');
        $this->shouldThrow(LockStorageException::class)->duringAdd($component);
    }

    function it_should_return_array_of_parameters()
    {
        $keys = ['api_key','signature'];

        $this->completeFillingUp('test', 'test');
        $this->data()->shouldHasKeys($keys);
    }
    
    function it_should_throw_empty_field_attribute_exception_if_set_empty_api_key()
    {
        $this->shouldThrow(EmptyFieldAttributeException::class)->duringCompleteFillingUp('', '');
    }

    public function getMatchers()
    {
        return [
            'hasKeys' => function($subject, $keys){
                $subjectKeys = array_keys($subject);
                foreach($keys as $key){
                    if(!in_array($key, $subjectKeys)){
                        return false;
                    }
                }
                return true;
            }
        ];
    }
}
