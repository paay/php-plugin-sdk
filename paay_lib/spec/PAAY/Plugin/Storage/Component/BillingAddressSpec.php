<?php

namespace spec\PAAY\Plugin\Storage\Component;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class BillingAddressSpec extends ObjectBehavior
{
    private $exampleData;
    
    function it_is_initializable()
    {
        $this->shouldHaveType('PAAY\Plugin\Storage\Component\BillingAddress');
    }

    function let()
    {
        $this->exampleData = array(
            'billingFirstName' => 'test',
            'billingLastName' => 'test',
            'billingCompany' => '',
            'billingEmail' => 'test',
            'billingAddress1' => 'test',
            'billingAddress2' => '',
            'billingCity' => 'test',
            'billingPostcode' => 'test',
            'billingState' => 'test',
            'billingCountry' => 'test',
            'billingPhone' => 'test'
        );
    }

    function it_should_return_true_id_component_data_are_valid()
    {
        $this->fill($this->exampleData)->shouldBe(true);
    }

    function it_should_return_array_of_parameters()
    {
        $keys = ['billingFirstName', 'billingLastName', 'billingCompany', 'billingEmail',
            'billingAddress1', 'billingAddress2', 'billingCity', 'billingPostcode', 'billingState',
            'billingCountry', 'billingPhone'];

        $this->fill($this->exampleData);
        $this->data()->shouldHasKeys($keys);
    }

    public function getMatchers()
    {
        return [
            'hasKeys' => function($subject, $keys){
                $subjectKeys = array_keys($subject);
                foreach($keys as $key){
                    if(!in_array($key, $subjectKeys)){
                        return false;
                    }
                }
                return true;
            }
        ];
    }
}
