<?php

namespace spec\PAAY\Plugin\Storage\Component;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ShippingAddressSpec extends ObjectBehavior
{
    private $exampleData;
    
    function it_is_initializable()
    {
        $this->shouldHaveType('PAAY\Plugin\Storage\Component\ShippingAddress');
    }

    function let()
    {
        $this->exampleData = array(
            'shippingFirstName' => 'test',
            'shippingLastName' => 'test',
            'shippingCompany' => 'test',
            'shippingAddress1' => 'test',
            'shippingAddress2' => 'test',
            'shippingCity' => 'test',
            'shippingPostcode' => 'test',
            'shippingState' => 'test',
            'shippingCountry' => 'test',
            'shippingPhone' => 'test'
        );
    }
    
    function it_should_return_true_id_component_data_are_valid()
    {
        $this->fill($this->exampleData)->shouldBe(true);
    }

    function it_should_return_array_of_parameters()
    {
        $keys = ['shippingFirstName', 'shippingLastName', 'shippingCompany', 'shippingAddress1', 
            'shippingAddress2', 'shippingCity', 'shippingPostcode', 'shippingState',
            'shippingCountry', 'shippingPhone'];

        $this->fill($this->exampleData);
        $this->data()->shouldHasKeys($keys);
    }

    public function getMatchers()
    {
        return [
            'hasKeys' => function($subject, $keys){
                $subjectKeys = array_keys($subject);
                foreach($keys as $key){
                    if(!in_array($key, $subjectKeys)){
                        return false;
                    }
                }
                return true;
            }
        ];
    }
}
