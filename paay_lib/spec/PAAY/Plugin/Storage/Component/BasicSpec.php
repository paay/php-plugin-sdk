<?php

namespace spec\PAAY\Plugin\Storage\Component;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class BasicSpec extends ObjectBehavior
{
    private $exampleData;
    
    function it_is_initializable()
    {
        $this->shouldHaveType('PAAY\Plugin\Storage\Component\Basic');
    }

    function let()
    {
        $this->exampleData = array(
            'amount' => 12.0,
            'orderId' => 1,
            'details' => '[{"name":"test","cost":"1.0"}]',
            'returnUrl' => 'http://example.com/return',
            'cancelUrl' => 'http://example.com/cancel',
            'statusUrl' => 'http://example.com/status',
            'email' => 'example@example.com',
            'threeds_visibility' => 'never'
        );
    }
    
    function it_should_return_true_id_component_data_are_valid()
    {
        $this->fill($this->exampleData)->shouldBe(true);
    }
    
    function it_should_return_array_of_parameters()
    {
        $keys = ['amount','orderId','details','returnUrl','cancelUrl','statusUrl','email','threeds_visibility'];
        
        $this->fill($this->exampleData);
        $this->data()->shouldHasKeys($keys);
    }

    public function getMatchers()
    {
        return [
            'hasKeys' => function($subject, $keys){
                $subjectKeys = array_keys($subject);
                foreach($keys as $key){
                    if(!in_array($key, $subjectKeys)){
                        return false;
                    }
                }
                return true;
            }
        ];
    }
}
