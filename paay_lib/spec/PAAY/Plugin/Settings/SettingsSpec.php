<?php

namespace spec\PAAY\Plugin\Settings;

use PAAY\Plugin\Settings\Settings;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use PAAY\Plugin\Exceptions\NoSettingFileException;
use PAAY\Plugin\Exceptions\ConfigFileException;

class SettingsSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('PAAY\Plugin\Settings\Settings');
    }
    
    function it_should_throw_no_settings_file_exception_if_yaml_file_does_not_exist()
    {
        $this->beConstructedWith('wrong_path_to_file');
        $this->shouldThrow(NoSettingFileException::class)->duringInstantiation();
    }

    function it_should_have_settings_property_after_initiation()
    {
        $this->settings()->shouldHaveKeys([
            Settings::AUTHORIZATION_FIELDS,
            Settings::BILLING_ADDRESS_FIELDS,
            Settings::SHIPPING_ADDRESS_FIELDS,
            Settings::AUTHORIZATION_FIELDS
        ]);
    }
    
    function it_should_throw_exception_if_settings_key_does_not_exist()
    {
        $this->shouldThrow(ConfigFileException::class)->duringSettings('wrong_key');
    }

    function it_settings_have_all_necessary_keys_in_fields()
    {
        $keys = ['required', 'type'];
        
        $this->settings(Settings::AUTHORIZATION_FIELDS)->shouldFieldHasKeys($keys);
        $this->settings(Settings::BILLING_ADDRESS_FIELDS)->shouldFieldHasKeys($keys);
        $this->settings(Settings::SHIPPING_ADDRESS_FIELDS)->shouldFieldHasKeys($keys);
        $this->settings(Settings::AUTHORIZATION_FIELDS)->shouldFieldHasKeys($keys);
    }

    public function getMatchers()
    {
        return [
            'haveKeys' => function($subject, $keys) {
                foreach($keys as $key) {
                    if(!array_key_exists($key, $subject)) {
                        return false;
                    }
                }
                return true;
            },
            'fieldHasKeys' => function($subject, $keys){
                if(!is_array($subject)){
                    return true;
                }
                foreach($keys as $key){
                    foreach ($subject as $field){
                        if(!array_key_exists($key, $field)){
                            return false;
                        }
                    }
                }
                return true;
            }
        ];
    }
}
