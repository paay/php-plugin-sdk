<?php

namespace spec\PAAY\Plugin\Validation;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use PAAY\Plugin\Exceptions\NoValidationTypeException;
use PAAY\Plugin\Exceptions\InvalidValueException;

class ValidatorSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('PAAY\Plugin\Validation\Validator');
    }
    
    function it_should_throw_exception_if_set_undefined_type()
    {
        $this->shouldThrow(NoValidationTypeException::class)->duringValid('undefined_type', 'random_value');
    }

    function it_should_return_true_if_set_correct_detail_type()
    {
        $details = json_encode(array(
            array(
                'name' => 'custom_name',
                'cost' => 1.1
            ))
        );
        $this->valid('details', $details)->shouldReturn(true);
    }
    
    function it_should_throw_exception_if_set_incorrect_details()
    {
        $details = json_encode(array('incorrect'));
        
        $this->shouldThrow(InvalidValueException::class)->duringValid('details', $details);
    }

    function it_should_return_true_if_set_correct_email_type()
    {
        $email = 'example@example.com';
        
        $this->valid('email', $email)->shouldReturn(true);
    }

    function it_should_throw_exception_if_set_incorrect_email()
    {
        $email = 'incorrect';

        $this->shouldThrow(InvalidValueException::class)->duringValid('email', $email);
    }

    function it_should_return_true_if_set_correct_money_type()
    {
        $money = 23.45;

        $this->valid('money', $money)->shouldReturn(true);
    }

    function it_should_throw_exception_if_set_incorrect_money()
    {
        $money = 'incorrect';

        $this->shouldThrow(InvalidValueException::class)->duringValid('money', $money);
    }
    
    function it_should_return_true_if_set_correct_text_type()
    {
        $text = 'some_text';

        $this->valid('text', $text)->shouldReturn(true);
    }

    function it_should_throw_exception_if_set_incorrect_text()
    {
        $text = '';

        $this->shouldThrow(InvalidValueException::class)->duringValid('text', $text);
    }

    function it_should_return_true_if_set_correct_url_type()
    {
        $url = 'http://example.com/example';

        $this->valid('url', $url)->shouldReturn(true);
    }

    function it_should_throw_exception_if_set_incorrect_url()
    {
        $url = 'http:example.com/example';

        $this->shouldThrow(InvalidValueException::class)->duringValid('url', $url);
    }
}
