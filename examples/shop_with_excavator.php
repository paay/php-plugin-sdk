<?php

require_once('../paay_lib_production/init.php');
require_once 'classes/ShopSettings.php';
require_once 'classes/ShopExcavator.php';

use PAAY\Plugin\PaayApiPlugin;

$settings = [
    'paay_key' => 'test_key',
    'paay_secret' => 'test_secret',
    'paay_payment_strategy' => 'redirect',
    'paay_cancel_url' => 'payment/custom/url',
];
$baseUrl = 'http://example.com';

$settings = new ShopSettings($settings, $baseUrl);

$order = array(
    'amount'    => 3.33,
    'orderId'   => 1,
    'details'   => array(
        array("name" => "Test Product 1", "amount" => 2, "cost" => 1.11),
        array("name" => "Test Product 2", "amount" => 1, "cost" => 2.22)
    ),
    'email'     => 'test@example.com',
    'billingFirstName'  => 'Jon',
    'billingLastName'   => 'Doe',
    'billingCompany'    => '',
    'billingEmail'      => 'test@example.com',
    'billingAddress1'   => 'Main Street',
    'billingAddress2'   => '11A',
    'billingCity'       => 'New York',
    'billingPostcode'   => '41165',
    'billingState'      => 'NY',
    'billingCountry'    => 'USA',
    'billingPhone'      => '111222333',
    'shippingFirstName' => 'Jon',
    'shippingLastName'  => 'Doe',
    'shippingAddress1'  => 'Main Street',
    'shippingAddress2'  => '11A',
    'shippingCity'      => 'New York',
    'shippingPostcode'  => '41165',
    'shippingState'     => 'NY',
    'shippingCountry'   => 'USA',
    'shippingPhone'     => '111222333',
);

$excavator = new ShopExcavator();

$excavator->setSettings($settings);
$excavator->setOrder($order);

$api = new PaayApiPlugin();
$api->setParameters($excavator);
$api->setAuthorizationParameters('merc_abc_key', 'merc_abc_secret');

echo $api->getTransactionUrl('http://api2.paay.dock');