<?php

require_once 'classes/ShopSettings.php';

$settings = [
    'paay_host' => 'http://api.paay.co',
    'paay_key' => 'test_key',
    'paay_secret' => 'test_secret',
    'paay_payment_strategy' => 'redirect',
    'paay_cancel_url' => 'payment/custom/url',
];
$baseUrl = 'http://example.com';

$settings = new ShopSettings($settings, $baseUrl);

echo $settings->host();
echo '<br>';
echo $settings->key();
echo '<br>';
echo $settings->secret();
echo '<br>';
echo $settings->paymentStrategy();
echo '<br>';
echo $settings->iframeOption();
echo '<br>';
echo $settings->returnUrl();
echo '<br>';
echo $settings->cancelUrl();
echo '<br>';
echo $settings->statusUrl();