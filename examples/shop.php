<?php

require_once('../paay_lib_production/init.php');

use PAAY\Plugin\PaayApiPlugin;

$basicParameters = array(
    'amount'    => 3.33,
    'orderId'   => 1,
    'details'   => json_encode(array(
        array("name" => "Test Product 1", "amount" => 2, "cost" => 1.11),
        array("name" => "Test Product 2", "amount" => 1, "cost" => 2.22)
    )),
    'returnUrl' => 'http://example.com/success',
    'cancelUrl' => 'http://example.com/cancel/1',
    'statusUrl' => 'http://example.com/approve_transaction?orderId=1',
    'email'     => 'test@example.com',
    'threeds_visibility' => 'detected',     // 'detected' or 'never'
);

$billingAddressParameters = array(
    'billingFirstName'  => 'Jon',
    'billingLastName'   => 'Doe',
    'billingCompany'    => '',
    'billingEmail'      => 'test@example.com',
    'billingAddress1'   => 'Main Street',
    'billingAddress2'   => '11A',
    'billingCity'       => 'New York',
    'billingPostcode'   => '41165',
    'billingState'      => 'NY',
    'billingCountry'    => 'USA',
    'billingPhone'      => '111222333',
);

$shippingAddressParameters = array(
    'shippingFirstName' => 'Jon',
    'shippingLastName'  => 'Doe',
    'shippingAddress1'  => 'Main Street',
    'shippingAddress2'  => '11A',
    'shippingCity'      => 'New York',
    'shippingPostcode'  => '41165',
    'shippingState'     => 'NY',
    'shippingCountry'   => 'USA',
    'shippingPhone'     => '111222333',
);

try {
    $api = new PaayApiPlugin();

    $api->setBasicParameters($basicParameters);
    $api->setBillingParameters($billingAddressParameters);
    $api->setShippingParameters($shippingAddressParameters);

    $api->setAuthorizationParameters('merc_abc_key', 'merc_abc_secret');

    echo $api->getTransactionUrl('http://api2.paay.dock');

} catch(\Exception $e){
    echo 'ERROR: ' . $e->getMessage();
}
