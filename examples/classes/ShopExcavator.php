<?php

require_once('../paay_lib_production/init.php');

use PAAY\Plugin\Helpers\DataExcavator;
use PAAY\Plugin\Helpers\DataExcavatorInterface;

class ShopExcavator extends DataExcavator implements DataExcavatorInterface
{
    private $order;
    
    public function setOrder($order)
    {
        $this->order = $order;    
    }
    
    public function excavateBasicParameters()
    {
        $shopData = array(
            'amount' => $this->order['amount'],
            'orderId' => $this->order['orderId'],
            'details' => $this->details($this->order['details']),
            'email' => $this->order['email']
        );

        return $this->fillSettingsField($shopData, $this->order['orderId']);
    }

    private function details(array $items)
    {
        $details = array();

        foreach($items as $item){
            $details[] = array(
                'name' => $item['name'],
                'cost' => $item['cost'],
                'amount' => $item['amount']
            );
        }

        return json_encode($details);
    }

    public function excavateBillingParameters()
    {
        return array(
            'billingFirstName'  => $this->order['billingFirstName'],
            'billingLastName'   => $this->order['billingLastName'],
            'billingCompany'    => $this->order['billingCompany'],
            'billingEmail'      => $this->order['billingEmail'],
            'billingAddress1'   => $this->order['billingAddress1'],
            'billingAddress2'   => $this->order['billingAddress2'],
            'billingCity'       => $this->order['billingCity'],
            'billingPostcode'   => $this->order['billingPostcode'],
            'billingState'      => $this->order['billingState'],
            'billingCountry'    => $this->order['billingCountry'],
            'billingPhone'      => $this->order['billingPhone'],
        );
    }

    public function excavateShippingParameters()
    {
        return array(
            'shippingFirstName'  => $this->order['shippingFirstName'],
            'shippingLastName'   => $this->order['shippingLastName'],
            'shippingAddress1'   => $this->order['shippingAddress1'],
            'shippingAddress2'   => $this->order['shippingAddress2'],
            'shippingCity'       => $this->order['shippingCity'],
            'shippingPostcode'   => $this->order['shippingPostcode'],
            'shippingState'      => $this->order['shippingState'],
            'shippingCountry'    => $this->order['shippingCountry'],
            'shippingPhone'      => $this->order['shippingPhone'],
        );
    }
}