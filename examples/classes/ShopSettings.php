<?php

require_once('../paay_lib_production/init.php');

use PAAY\Plugin\Helpers\Settings;
use PAAY\Plugin\Helpers\SettingsInterface;

class ShopSettings extends Settings implements SettingsInterface
{
    const PAAY_HOST             = 'paay_host';
    const PAAY_KEY              = 'paay_key';
    const PAAY_SECRET           = 'paay_secret';
    const PAAY_PAYMENT_STRATEGY = 'paay_payment_strategy';
    const PAAY_IFRAME_OPTION    = 'paay_iframe_option';
    const PAAY_RETURN_URL       = 'paay_return_url';
    const PAAY_CANCEL_URL       = 'paay_cancel_url';
    const PAAY_STATUS_URL       = 'paay_status_url';

    const RETURN_URL = 'transaction/success';
    const CANCEL_URL = 'it will not be used';
    const STATUS_URL = 'transaction/approve_transaction';

    public function __construct($settings, $baseUrl)
    {
        $this->settings = $settings;
        $this->baseUrl = $baseUrl;
    }
}