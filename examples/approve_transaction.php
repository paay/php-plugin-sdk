<?php

require_once('../paay_lib_production/init.php');
require_once 'classes/ShopSettings.php';

$paayApi = new PaayApiPlugin();

// settings object is only example in real app you should use your own settings class
$settings = new ShopSettings(array(), null);

// you should't use POST data directly
$post = $_POST;
$signature = (isset($post['signature'])) ? $post['signature'] : null;


if(!$paayApi->checkSignature($settings->key(), $settings->secret(), $post, $signature)){
    throw new \Exception('You are Unauthorized');
}

echo 'OK';