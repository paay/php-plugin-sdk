<?php

namespace PAAY\Plugin\Storage\Component;

use PAAY\Plugin\Settings\Settings;

class BillingAddress extends StorageComponent implements StorageComponentInterface
{
    protected $key = Settings::BILLING_ADDRESS_FIELDS;    
}
