<?php

namespace PAAY\Plugin;

interface PaayApiPluginInterface
{
    public function setBasicParameters(array $data);

    public function setBillingParameters(array $data);

    public function setShippingParameters(array $data);

    public function setAuthorizationParameters($key, $secret);

    public function getTransactionUrl($host, $strategy);

    public function checkSignature($key, $secret, $payload, $signature);
}