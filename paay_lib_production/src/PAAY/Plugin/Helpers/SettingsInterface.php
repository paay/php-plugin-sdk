<?php

namespace PAAY\Plugin\Helpers;

interface SettingsInterface
{
    public function host();
    
    public function key();
    
    public function secret();
    
    public function paymentStrategy();
    
    public function iframeOption();
    
    public function returnUrl();
    
    public function cancelUrl();
    
    public function statusUrl();
}