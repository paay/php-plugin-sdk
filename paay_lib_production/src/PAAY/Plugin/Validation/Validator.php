<?php

namespace PAAY\Plugin\Validation;

use PAAY\Plugin\Exceptions\NoValidationTypeException;
use PAAY\Plugin\Exceptions\InvalidValueException;

class Validator
{
    private $componentNamespace = 'PAAY\\Plugin\\Validation\\Components';
    
    public function valid($type, $value, $name = 'undefined')
    {
        $className = $this->componentNamespace .'\\'. ucfirst($type);
        if(!class_exists($className)){
            throw new NoValidationTypeException("Validation class '{$className}' does not exist");
        }

        $class = new $className();
        if(!$class->valid($value)){
            $message = "Value with key '{$name}' is empty or incorrect. Please make sure your plugin setting are correct";
            $message .= (strlen($value) === 0) ? " (value is empty)." : " (value '{$value}' is incorrect).";
            throw new InvalidValueException($message);
        }

        return true;
    }
}
