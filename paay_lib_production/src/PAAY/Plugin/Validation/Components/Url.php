<?php

namespace PAAY\Plugin\Validation\Components;

class Url implements ValidatorInterface
{
    public function valid($value)
    {
        if (filter_var($value, FILTER_VALIDATE_URL)){
            return true;
        }

        return false;
    }
}