<?php

namespace PAAY\Plugin\Validation\Components;

class Details implements ValidatorInterface
{
    public function valid($value)
    {
        return $this->is_correct($value);
    }

    private function is_correct($str)
    {
        if(!$this->is_json($str)){
            return false;
        }
        $details = json_decode($str, true);

        foreach($details as $element){
            if(!is_array($element) || empty($element['name']) || empty($element['cost'])){
                return false;
            }
        }

        return true;
    }

    private function is_json($str)
    {
        return json_decode($str) != null;
    }
}
