<?php

namespace PAAY\Plugin\Validation\Components;

class Money implements ValidatorInterface
{
    public function valid($value)
    {
        if (filter_var($value, FILTER_VALIDATE_FLOAT)){
            return true;
        }
        return false;
    }
}