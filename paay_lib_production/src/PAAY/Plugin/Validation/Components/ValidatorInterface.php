<?php

namespace PAAY\Plugin\Validation\Components;

interface ValidatorInterface
{
    public function valid($value);
}