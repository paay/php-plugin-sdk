<?php

namespace PAAY\Plugin\Settings;

use Symfony\Component\Yaml\Yaml;
use PAAY\Plugin\Exceptions\NoSettingFileException;
use PAAY\Plugin\Exceptions\ConfigFileException;

class Settings
{
    private $settings = array();
    private $path;
    
    const BASIC_FIELDS = 'basic_fields';
    const BILLING_ADDRESS_FIELDS = 'billing_address_fields';
    const SHIPPING_ADDRESS_FIELDS = 'shipping_address_fields';
    const AUTHORIZATION_FIELDS = 'authorization_fields';

    public function __construct($path = null)
    {
        $this->path = (empty($path)) ? $this->path() : $path;
        $this->init();
    }

    private function init()
    {
        if(!file_exists($this->path)){
            throw new NoSettingFileException('Settings file does not exist');
        }

        $this->settings = Yaml::parse(file_get_contents($this->path));
    }

    private function path()
    {
        return dirname(__FILE__) . '/../../../../config/settings.yml';
    }

    public function settings($key = null)
    {
        if($key === null){
            return $this->settings;
        }

        if(!array_key_exists($key, $this->settings)){
            throw new ConfigFileException("Settings with key: \"{$key}\" does not exist");
        }

        return $this->settings[$key];
    }
}
